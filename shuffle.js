
module.exports = function (xs) {
  for (let i = xs.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1))
    let tmp = xs[i]
    xs[i] = xs[j]
    xs[j] = tmp
  }
}
